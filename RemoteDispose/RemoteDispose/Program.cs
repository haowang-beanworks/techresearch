﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteDispose {
    class RemoteClass : MarshalByRefObject {
        private Timer timer;
        public void Hold() {
            var state = new object();
            timer = new Timer(
                callback: TimerTask,
                state: state,
                dueTime: 5000,
                period: 10000);
        }
        public string Echo(string s) {
            return s;
        }

        static void TimerTask(object state) {
            Console.WriteLine("Remote Class Running" + DateTime.Now);
        }
    }

    class Program {
        static RemoteClass rc;
        static TimeSpan time;
        static Timer timer;

        static void Main(string[] args) {
            AppDomain ad = AppDomain.CreateDomain("OtherDomain");
            rc = (RemoteClass)ad.CreateInstanceAndUnwrap(
                Assembly.GetExecutingAssembly().FullName,
                typeof(RemoteClass).FullName);
            rc.Hold();
            time = TimeSpan.FromMinutes(6);
            timer = new Timer((o) => {
                time = time.Subtract(TimeSpan.FromSeconds(5));
                Console.WriteLine(time);

                if (time.TotalSeconds <= 1) {
                    try {
                        Console.WriteLine(rc.Echo("Hello"));
                    } catch (Exception e) {
                        Console.WriteLine(e.Message);
                    }
                }

            });
            timer.Change(0, 5000);
            Console.ReadLine();
        }
    }
}
